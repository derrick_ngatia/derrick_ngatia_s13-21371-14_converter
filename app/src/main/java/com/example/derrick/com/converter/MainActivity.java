package com.example.derrick.com.converter;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends Activity {
    TextView result;
    EditText millimetres,inches;
    Button calculate,exit;
    RadioButton to_inches,to_mms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result=(TextView)findViewById(R.id.result);
        millimetres=(EditText)findViewById(R.id.mms);
        inches=(EditText)findViewById(R.id.inches);
        calculate=(Button)findViewById(R.id.convert);
        exit=(Button)findViewById(R.id.exit);
        to_inches=(RadioButton) findViewById(R.id.to_inches);
        to_mms=(RadioButton)findViewById(R.id.to_mms);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (to_inches.isChecked()){
                    if(!millimetres.getText().toString().isEmpty()){
                        Double inche=Double.valueOf(millimetres.getText().toString())/25.4;
                        inches.setText(inche.toString());
                        result.setText(millimetres.getText().toString()+" millimeters is equal to "+inche+" inches");
                    }else{
                        millimetres.setError("millimeters to convert required");
                    }
                }else{
                    if(!inches.getText().toString().isEmpty()){
                        Double mms=Double.valueOf(inches.getText().toString()) * 25.4;
                        millimetres.setText(mms.toString());
                        result.setText(inches.getText()+" inches is equal to "+mms+" millimeters");
                    }else{
                        inches.setError("inches to convert required");
                    }

                }
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
